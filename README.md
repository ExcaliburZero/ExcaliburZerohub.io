#ExcaliburZero.github.io
This is the GitHub repo for ExcaliburZero's website.

##Build Status
[![Build Status](https://travis-ci.org/ExcaliburZero/ExcaliburZero.github.io.png)](https://travis-ci.org/ExcaliburZero/ExcaliburZero.github.io)

##Website URL
- http://excaliburzero.github.io/

##Site Map
- **index.html** - Site's main page. Includes basic information.
- **art.html** - Includes information about my art including a few examples.
- **website.html** - Includes a list of the websites I use and my accounts on those websites. Also includes more detailed descriptions of my actions on some of the important websites on the list.
- **contact.html** - Includes some basic contact information including my Gmail address.
